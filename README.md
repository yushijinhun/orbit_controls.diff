from: https://github.com/mrdoob/three.js/blob/d45a042cf962e9b1aa9441810ba118647b48aacb/examples/js/controls/OrbitControls.js

to: https://github.com/to2mbn/skinview3d/blob/a654cd20e1ad5d65e442d628e5c00cc7c06bc9f9/src/orbit_controls.js

use `git show --ignore-all-space <commit>` to view changes

only `fb4e416` made functional changes. it's diff is in `main.diff`.
